/**
 * @module zane.mvc
 */
module zane.mvc
{
    /**
     * @class zane.mvc.IControllerClass
     */
    export interface IControllerClass
    {
        new (cmd:string) : Controller;

    }
}