/**
 * @module zane.mvc
 */
module zane.mvc
{
    /**
     * @class zane.mvc.IViewClass
     */
    export interface IViewClass
    {
        new (name:string, viewComponent:Object) : View;
    }
}