/**
 * @module zane.mvc
 */
module zane.mvc
{
    /**
     * @class zane.mvc.IModelClass
     */
    export interface IModelClass
    {
        new (name:string, data?:any) : Model;
    }
}