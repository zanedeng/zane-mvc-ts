/**
 * @module zane.mvc
 */
module zane.mvc
{
    /**
     * @class zane.mvc.Model
     */
	export class Model
	{

        ////////////////////////////////////////////////////////////////////////////
        // public static property
        ////////////////////////////////////////////////////////////////////////////

        public static modelList:Model[] = [];

        ////////////////////////////////////////////////////////////////////////////
        //  public static methods
        ////////////////////////////////////////////////////////////////////////////

        public static retrieveModel(name:string):Model
        {
            var len:number = Model.modelList.length;
            for (var i:number = 0; i < len; ++i)
            {
                if (Model.modelList[i].name == name)
                {
                    return Model.modelList[i];
                }
            }
            return null;
        }

        public static removeModel(name:string):void
        {
            var len:number = Model.modelList.length;
            for (var i:number = 0; i < len; ++i)
            {
                if (Model.modelList[i].name == name)
                {
                    Model.modelList[i].onRemove();
                    Model.modelList[i].data = null;
                    Model.modelList[i] = null;
                    Model.modelList.splice(i, 1);
                    break;
                }
            }
        }

        ////////////////////////////////////////////////////////////////////////////
        // public property
        ////////////////////////////////////////////////////////////////////////////

        public name:string;
        public data:Object = {};

        ////////////////////////////////////////////////////////////////////////////
        //  constructor
        ////////////////////////////////////////////////////////////////////////////

        constructor(name:string, data:Object = null)
		{
            if (name == undefined || name == "")
                throw new Error("Model name can not undefined!");
            if (Model.retrieveModel(name) != null)
                throw new Error("Model[" + name +"]" + " instance  already constructed !");
            this.name = name;
            if (data != null)
            {
                for (var i in data)
                    this.data[i] = data[i];
            }

            Model.modelList.push(this);
            this.onRegister();
		}

        ////////////////////////////////////////////////////////////////////////////
        //  public methods
        ////////////////////////////////////////////////////////////////////////////

        public onRegister():void { }

        public onRemove():void { }

        public sendEvent(type:string, data:any = null):void
        {
            View.notifyViews(type, data, this);
        }
	}
}